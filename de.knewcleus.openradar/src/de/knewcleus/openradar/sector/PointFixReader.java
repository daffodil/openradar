package de.knewcleus.openradar.sector;

import de.knewcleus.fgfs.location.Position;
import de.knewcleus.fgfs.navaids.DesignatedPoint;
import de.knewcleus.fgfs.navaids.NavDB;

public class PointFixReader extends PointReader {
	@Override
	protected void processLine(NavDB navDB, Position pos, String id, String rest) {
		
		DesignatedPoint designatedPoint = new DesignatedPoint(id, pos);
		
		navDB.addRecord(designatedPoint);
	}
}

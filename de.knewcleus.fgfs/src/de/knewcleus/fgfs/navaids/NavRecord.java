package de.knewcleus.fgfs.navaids;

import de.knewcleus.fgfs.location.Position;


public interface NavRecord {
	public String getID();
	public Position getPosition();
	public String getXType();
}

package de.knewcleus.openradar.ui.rpvd;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.LayoutManager;
import java.awt.Dialog.ModalityType;
//import java.awt.Window;s

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import java.awt.event.WindowStateListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;



import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.JFrame;



import de.knewcleus.fgfs.Units;
import de.knewcleus.fgfs.location.IMapProjection;
import de.knewcleus.fgfs.util.GeometryConversionException;
import de.knewcleus.openradar.ui.RadarWorkstation;
import de.knewcleus.openradar.ui.WorkstationGlobalFrame;
import de.knewcleus.openradar.ui.map.RadarMapPanel;
import de.knewcleus.openradar.ui.rpvd.toolbox.MapMenuFrame;
import de.knewcleus.openradar.ui.rpvd.toolbox.OverlapMenuFrame;
import de.knewcleus.openradar.ui.rpvd.toolbox.RadarToolbox;
import de.knewcleus.openradar.ui.rpvd.toolbox.SpeedAndTrackPanel;
import de.knewcleus.openradar.ui.rpvd.toolbox.ZoomPanel;

/** Displays the radar and its setup
 *  abd the controls at the bottom

 *
 */
public class RadarPlanViewDisplay extends WorkstationGlobalFrame implements PropertyChangeListener, ComponentListener {
	private static final long serialVersionUID = 5923481231980915972L;
	
	
	protected final JDesktopPane subDesktopPane = new JDesktopPane();
	protected final RadarMapPanel radarMapPanel;
	protected final RadarToolbox radarToolbox;

	protected final JToggleButton mapOptionsButton;
	//JDialog mapOptionsDialog;
	protected final MapMenuFrame mapMenuFrame;
	protected final JPanel zoomPanel;
	protected final JPanel speedAndTrackPanel;
	
	//JDialog overlapDialog;
	protected final JToggleButton overlapOptionsButton;
	protected final OverlapMenuFrame overlapMenuFrame;
	
	protected final Integer RPVD_LAYER = 0;
	protected final Integer DESKTOP_LAYER = 1;
	
	public RadarPlanViewDisplay(RadarWorkstation workstation, IMapProjection mapTransformation) throws GeometryConversionException {
		super(workstation, "RPVD#", "Radar SCAN #1", true, false, true, false);
		
		setLayout(new BorderLayout());
		//Container contentPanel = getContentPane();
		
		radarMapPanel = new RadarPlanViewPanel(workstation, mapTransformation);

		//setLayout(new OverlayLayout());
		//add(subDesktopPane, BorderLayout.SOUTH);
		add(radarMapPanel, BorderLayout.CENTER);
		
		
		//=====================================================
		//=== Bottom Bar + Options ===
        JToolBar toolbar = new JToolBar();
        add(toolbar, BorderLayout.SOUTH );
        toolbar.setVisible(true);
		
        //== Map Options Button
        mapOptionsButton = new JToggleButton("Map Options");
        toolbar.add(mapOptionsButton);
        mapOptionsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	Point p = getContentPane().getMousePosition();
            	mapMenuFrame.move(p.x, p.y);
            	mapMenuFrame.setVisible( mapOptionsButton.isSelected() );
            	
            }
        });
        toolbar.addSeparator();

        //== Marker Options
        overlapOptionsButton = new JToggleButton("Markers");
        toolbar.add(overlapOptionsButton);
        overlapOptionsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	Point p = getContentPane().getMousePosition();
            	overlapMenuFrame.move(p.x, p.y);
            	overlapMenuFrame.setVisible( true );
            	
            }
        });
        
        
        //== Zoom Panel
        zoomPanel = new ZoomPanel(this);
        toolbar.add(zoomPanel);
        
        //== Speed and Track
        speedAndTrackPanel = new SpeedAndTrackPanel(this);
        toolbar.add(speedAndTrackPanel);
        

        
        //===============================
           
        
        
		radarToolbox = new RadarToolbox(this);
		radarToolbox.setVisible(false);
		add(radarToolbox, BorderLayout.EAST);
		//setPreferredSize(new Dimension(100, 100));
		
		//subDesktopPane.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		//subDesktopPane.setOpaque(false);
		
		//subDesktopPane.add(radarToolbox);
		
		//try {
		//	radarToolbox.setIcon(true);
		//} catch (PropertyVetoException e) {
		//	// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		
		workstation.getRadarPlanViewSettings().addPropertyChangeListener(this);
		radarMapPanel.addComponentListener(this);
		
		//=== Map Options Dialog
		//mapOptionsDialog = new JDialog(new JFrame(), "Map Layers", false);
		//mapOptionsDialog.setResizable(false);
		//BorderLayout bl = new BorderLayout();

		//mapOptionsDialog.getContentPane().setLayout(new BorderLayout());
		//mapOptionsDialog.set
		//mapOptionsDialog.setModalityType(ModalityType.TOOLKIT_MODAL);
		
		mapMenuFrame = new MapMenuFrame(this);
		mapMenuFrame.setVisible(false);
		mapMenuFrame.addWindowListener(new WindowListener() {
			
	          public void windowActivated(WindowEvent e){}
	          public void windowOpened(WindowEvent e){}
	          public void windowIconified(WindowEvent e){}
	          public void windowClosing(WindowEvent e){}
	          public void windowDeactivated(WindowEvent e){
	        	  mapOptionsButton.setSelected(false);
	          }
	          public void windowDeiconified(WindowEvent e){}
	          public void windowClosed(WindowEvent e){}
	    });	
		
		//overlapDialog = new JDialog();
		overlapMenuFrame = new OverlapMenuFrame(this);
		overlapMenuFrame.setPreferredSize(new Dimension(120, 300));
		overlapMenuFrame.setVisible(false);
		overlapMenuFrame.addWindowListener(new WindowListener() {
			
	          public void windowActivated(WindowEvent e){}
	          public void windowOpened(WindowEvent e){}
	          public void windowIconified(WindowEvent e){}
	          public void windowClosing(WindowEvent e){}
	          public void windowDeactivated(WindowEvent e){
	        	  overlapOptionsButton.setSelected(false);
	        	  
	          }
	          public void windowDeiconified(WindowEvent e){}
	          public void windowClosed(WindowEvent e){}
	    });	
		
		//overlapDialog.getContentPane().add(overlapMenuFrame);
		//overlapDialog.setVisible(false);
		
		//add(overlapMenuFrame, BorderLayout.WEST);
		
		//mapMenuFrame.add(new ActionListener() {
        //    public void actionPerformed(ActionEvent event) {
        //   	
		//mapOptionsButton.setToolTipText("toogle me"text)
         //   }
		//});
		pack();
	}
	
	public JDesktopPane getSubDesktopPane() {
		return subDesktopPane;
	}
	
	public RadarMapPanel getRadarMapPanel() {
		return radarMapPanel;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource()==radarMapPanel.getSettings() &&
				evt.getPropertyName().equals(RadarPlanViewSettings.RANGE_PROPERTY)) {
			updateScale();
		}
	}
	
	protected void updateScale() {
		final int viewWidth=radarMapPanel.getVisibleRect().width;
		if (viewWidth==0)
			return;
		final RadarPlanViewSettings settings=radarMapPanel.getSettings();

		final double xRange=settings.getRange()*Units.NM;
		radarMapPanel.setScale(viewWidth/xRange);
	}

	@Override
	public void componentResized(ComponentEvent e) {
		updateScale();
	}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentShown(ComponentEvent e) {}
	
	class OverlayLayout implements LayoutManager {
		@Override
		public void addLayoutComponent(String name, Component comp) {
		}
		
		@Override
		public void removeLayoutComponent(Component comp) {
		}
		
		@Override
		public void layoutContainer(Container parent) {
			Dimension size=parent.getSize();
			
			for (Component c: parent.getComponents()) {
				c.setBounds(0, 0, size.width, size.height);
			}
		}
		
		@Override
		public Dimension minimumLayoutSize(Container parent) {
			int maxWidth=0,maxHeight=0;
			
			for (Component c: parent.getComponents()) {
				Dimension minSize=c.getMinimumSize();
				maxWidth=Math.max(maxWidth, minSize.width);
				maxHeight=Math.max(maxHeight, minSize.height);
			}
			return new Dimension(maxWidth,maxHeight);
		}
		
		@Override
		public Dimension preferredLayoutSize(Container parent) {
			int maxWidth=0,maxHeight=0;
			
			for (Component c: parent.getComponents()) {
				Dimension minSize=c.getPreferredSize();
				maxWidth=Math.max(maxWidth, minSize.width);
				maxHeight=Math.max(maxHeight, minSize.height);
			}
			return new Dimension(maxWidth,maxHeight);
		}
	};
}

package de.knewcleus.fgfs.navaids;

import de.knewcleus.fgfs.location.Position;

public class DesignatedPoint extends AbstractNavRecord {
	public DesignatedPoint(String name, Position position) {
		super("FIX", name, position );
	}
}

package de.knewcleus.fgfs.multiplayer.protocol;

public enum PropertyType {
	NONE, 
	ALIAS,
	BOOL,
	INT,
	LONG,
	FLOAT,
	DOUBLE,
	STRING,
	UNSPECIFIED;
}

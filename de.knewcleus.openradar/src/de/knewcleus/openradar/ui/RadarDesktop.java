package de.knewcleus.openradar.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.awt.Container;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;
import javax.swing.Timer;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JLabel;
import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.awt.event.KeyEvent;

public class RadarDesktop extends JFrame {
	
	private static final long serialVersionUID = 8819041738184510314L;
	
	final  RadarWorkstation workstation;
	
	protected final JDesktopPane desktopPane = new JDesktopPane();
	
	//== Clock
	public JLabel clockLabel;
	protected final Timer clockUpdateTimer;
	protected final TimeZone timeZone = TimeZone.getTimeZone("GMT"); //??? UTC ??
	protected final Calendar calendar = Calendar.getInstance(timeZone);
	//protected final GeneralToolbox generalToolbox;
	
	//== Radio
    protected final JTextField activeFrequency = new JTextField();
	protected final JButton swapButton = new JButton("<< - >>");
	protected final JTextField standbyFrequency = new JTextField();
	final JLabel activeLabel = new JLabel("Active:");
	final JLabel standbyLabel = new JLabel("Standby:");
	
	 JToggleButton mpConnectButton; 
	 
	protected Rectangle lastRPVDBounds;
	protected final Map<WorkstationGlobalFrame, Rectangle> globalFrameBounds = new HashMap<WorkstationGlobalFrame, Rectangle>();
	
	public RadarDesktop(GraphicsConfiguration gc, RadarWorkstation workstation) {
		super("OpenRadar - pedro-dev", gc);
		this.workstation = workstation;
		
		
		//setContentPane(desktopPane);
		 Container pane = getContentPane();
		 pane.setLayout(new BorderLayout());
		 JLabel label = new JLabel("Bottom bit", JLabel.CENTER);
		 //label.setFont(new Font("Courier", Font.BOLD, 36));
		 //label.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		 pane.add(label, BorderLayout.SOUTH);
		    
		
		//getContentPane().setLayout(new BorderLayout());
		
		//== Setup Menu ==
		JMenuBar menubar = new JMenuBar();
        //ImageIcon icon = new ImageIcon(getClass().getResource("exit.png"));

        JMenu file = new JMenu("File");
        //file.setMnemonic(KeyEvent.VK_F);

        JMenuItem eMenuItem = new JMenuItem("Exit");
        //eMenuItem.setMnemonic(KeyEvent.VK_C);
        eMenuItem.setToolTipText("Exit application");
        eMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }

        });

        file.add(eMenuItem);

        menubar.add(file);

        setJMenuBar(menubar);

        //====================================
        //== Top Toolbar
        JToolBar toolbar = new JToolBar();
        pane.add(toolbar, BorderLayout.NORTH );
        toolbar.setVisible(true);
        
        toolbar.add(activeLabel);
        toolbar.add(activeFrequency);
        toolbar.add(swapButton);
        toolbar.add(standbyLabel);
        toolbar.add(standbyFrequency);
    
        //== MP Connect Button
        mpConnectButton = new JToggleButton("MP Ena");
        toolbar.add(mpConnectButton);
        mpConnectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                getWorkstation().run_mp(mpConnectButton.isSelected());
            }

        });
        
        //== Quit Button
        JButton exitButton = new JButton("Quit");
        toolbar.add(exitButton);
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }

        });
        toolbar.addSeparator();
       
        

    
    		
        //== Clock Label
        clockLabel = new JLabel("CLOCK");
        toolbar.add(clockLabel);
        

        
        
		
		desktopPane.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		//ssdesktopPane.setPreferredSize(new Dimension(900, 700));
		desktopPane.setOpaque(false);
		//pane.add(desktopPane, BorderLayout.CENTER );
		//generalToolbox = new GeneralToolbox(this);
		
		//generalToolbox.setVisible(true);
		//add(generalToolbox, JLayeredPane.PALETTE_LAYER);
		
		setPreferredSize(new Dimension(900, 700));
		pack();
		
		//== Clock Timer
		clockUpdateTimer = new Timer(1000, new ActionListener() {
		      public void actionPerformed(ActionEvent e) {
	    	  		calendar.setTimeInMillis(System.currentTimeMillis());
					int hour, minute, second;
					hour = calendar.get(Calendar.HOUR_OF_DAY);
					minute = calendar.get(Calendar.MINUTE);
					second = calendar.get(Calendar.SECOND);
					String timeString = String.format("%02d:%02d:%02d", hour, minute, second);
					clockLabel.setText(timeString);
		          //repaint();
		        }
		});
		clockUpdateTimer.setInitialDelay(1000); // start at one
		clockUpdateTimer.start();
		
	}
	
	public RadarWorkstation getWorkstation() {
		return workstation;
	}
	
	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}
	
	//public GeneralToolbox getGeneralToolbox() {
	//	return generalToolbox;
	//}
	
	public Rectangle getGlobalFrameBounds(WorkstationGlobalFrame frame) {
		return globalFrameBounds.get(frame);
	}
	
	public void setGlobalFrameBounds(WorkstationGlobalFrame frame, Rectangle bounds) {
		globalFrameBounds.put(frame, bounds);
	}
}

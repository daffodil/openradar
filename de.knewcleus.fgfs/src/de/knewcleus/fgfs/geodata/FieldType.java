package de.knewcleus.fgfs.geodata;

public enum FieldType {
	UNDEFINED,
	CHARACTER,
	NUMBER,
	LOGICAL,
	DATE,
	INTEGER;
}

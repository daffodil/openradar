package de.knewcleus.fgfs.multiplayer;

import static de.knewcleus.fgfs.multiplayer.protocol.MultiplayerPacket.MAX_PACKET_SIZE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.net.InetAddress;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.net.SocketException;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import de.knewcleus.fgfs.multiplayer.protocol.MultiplayerPacket;
import de.knewcleus.fgfs.multiplayer.protocol.XDRInputStream;
import de.knewcleus.fgfs.multiplayer.protocol.XDROutputStream;

public abstract class AbstractMultiplayerEndpoint<T extends Player> implements Runnable {
	
	protected static Logger logger = Logger.getLogger("de.knewcleus.fgfs.multiplayer");
	protected  DatagramSocket datagramSocket;
	protected final IPlayerRegistry<T> playerRegistry;
	protected final int timeoutMillis = 5000;

	protected volatile boolean  thread_suspended;
	
	//private void setSocketTimeout(){
		
		//datagramSocket.setSoTimeout(timeoutMillis);
	//}
	/*
	public AbstractMultiplayerEndpoint(IPlayerRegistry<T> playerRegistry) throws IOException {
		datagramSocket = new DatagramSocket();
		datagramSocket.setSoTimeout(timeoutMillis);
		this.playerRegistry = playerRegistry;
	}
	*/
	public AbstractMultiplayerEndpoint(IPlayerRegistry<T> playerRegistry, int port){ // throws IOException {
		this.playerRegistry = playerRegistry;
		datagramSocket = null;
		
		try{
			
			if (port == 0) {
				datagramSocket = new DatagramSocket();
			} else {
				datagramSocket = new DatagramSocket(port);
				//datagramSocket.setSoTimeout(timeoutMillis);
			}
			datagramSocket.setSoTimeout(timeoutMillis);
		}catch (SocketException e) {
				System.out.println("SocketException: "
                        + e.getMessage());
				
				//throw new SampleException(e);)
		}finally{
			
		}
		
	}
	
	public InetAddress getAddress() {
		return datagramSocket.getLocalAddress();
	}
	
	public int getPort() {
		return datagramSocket.getPort();
	}
	public void set_running(boolean state){
		this.thread_suspended = !state;
		Thread myThread = Thread.currentThread();
		if(state == true){
			myThread.start();
		}else{
			myThread.stop();
		}
	}
	
	@Override
	public void run() {
		Thread myThread = Thread.currentThread();
		//if (thread_suspended) {
            //    synchronized(this) {
            //        while (thread_suspended)
                        //wait();
           //     }
			
		//while (true) {
			while (!myThread.isInterrupted()) {
			//}
				
				try {
					//if (thread_suspended) {
					//	synchronized(this) {
					//		while (thread_suspended)
					//			myThread.wait();
					//	}
	            	//}
					if (thread_suspended) {
						break;
	            	}
					receivePacket();
				} catch (SocketException e) {
					/* ignore */
					
				} catch (SocketTimeoutException e) {
					/* ignore */
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MultiplayerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				//} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();		
				}

			expirePlayers();
		}
	}
	
	protected void receivePacket() throws IOException, MultiplayerException {
		//if(datagramSocket == null){
		//	return;
		//}
		byte[] buffer = new byte[MAX_PACKET_SIZE];
		final DatagramPacket packet = new DatagramPacket(buffer, MAX_PACKET_SIZE);
		datagramSocket.receive(packet);

		ByteArrayInputStream byteInputStream;
		XDRInputStream xdrInputStream;
		byteInputStream = new ByteArrayInputStream(packet.getData(), packet.getOffset(), packet.getLength());
		xdrInputStream = new XDRInputStream(byteInputStream);
		
		try{
			MultiplayerPacket mppacket = MultiplayerPacket.decode(xdrInputStream);
			processPacket(packet.getAddress(), packet.getPort(), mppacket);
			
		}catch(MultiplayerException e){
			//System.out.print("OOPS");
		}
	}
	
	protected void sendPacket(InetAddress address, int port, MultiplayerPacket mppacket) throws MultiplayerException {
		if(datagramSocket == null){
			return;
		}
		ByteArrayOutputStream byteOutputStream=new ByteArrayOutputStream(MAX_PACKET_SIZE);
		XDROutputStream xdrOutputStream=new XDROutputStream(byteOutputStream);
		mppacket.encode(xdrOutputStream);
		byte[] buffer=byteOutputStream.toByteArray();
		DatagramPacket packet=new DatagramPacket(buffer,buffer.length);
		packet.setAddress(address);
		packet.setPort(port);
		
		try {
			datagramSocket.send(packet);
		} catch (IOException e) {
			throw new MultiplayerException(e);
		}
	}
	
	protected void sendPacket(Player player, MultiplayerPacket mppacket) throws MultiplayerException {
		sendPacket(player.getAddress(), player.getPort(), mppacket);
	}

	protected void processPacket(InetAddress address, int port, MultiplayerPacket mppacket) throws MultiplayerException {
		T player;
		final String callsign = mppacket.getCallsign();
		synchronized (playerRegistry) {
			if (playerRegistry.hasPlayer(callsign)) {
				player = playerRegistry.getPlayer(callsign);
			} else {
				player = playerRegistry.createNewPlayer(mppacket.getCallsign());
				playerRegistry.registerPlayer(player);
				newPlayerLogon(player);
			}
		}
		player.setExpiryTime(System.currentTimeMillis() + timeoutMillis);
		player.setAddress(address);
		player.setPort(port);
		processPacket(player, mppacket);
	}
	
	protected void expirePlayers() {
		synchronized (playerRegistry) {
			Set<T> expiredPlayers=new HashSet<T>();
			long currentTime=System.currentTimeMillis();
			for (T player: playerRegistry.getPlayers()) {
				if (player.getExpiryTime()<=currentTime) {
					expiredPlayers.add(player);
				}
			}
			
			for (T player: expiredPlayers) {
				playerRegistry.unregisterPlayer(player);
			}
		}
	}
	
	protected void newPlayerLogon(T player) throws MultiplayerException {
	}

	protected abstract void processPacket(T player, MultiplayerPacket mppacket) throws MultiplayerException;
}

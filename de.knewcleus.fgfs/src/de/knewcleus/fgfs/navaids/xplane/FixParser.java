package de.knewcleus.fgfs.navaids.xplane;


import java.awt.Shape;

import de.knewcleus.fgfs.location.Position;
import de.knewcleus.fgfs.navaids.DBParserException;
import de.knewcleus.fgfs.navaids.DesignatedPoint;
import de.knewcleus.fgfs.navaids.NavDB;
/**
 * Parses xplane's fix.dat lines
 *
 */
public class FixParser extends AbstractXPlaneParser {
	protected final NavDB navDB;

	public FixParser(NavDB navDB, Shape geographicBounds)
	{
		super(geographicBounds);
		this.navDB = navDB;
	}
	
	@Override
	protected void processRecord(String line) throws DBParserException {
		
		// eg: 22.528056 -156.170961 00MKK
		String[] tokens = line.split("\\s+", 3);
		
		//= Check lat, lng in range
		double lon, lat;
		lat = Double.parseDouble(tokens[0]);
		lon = Double.parseDouble(tokens[1]);
		
		if (!isInRange(lon, lat))
			return;
		
		Position pos = new Position(lon, lat, 0.0);
		
		navDB.addRecord(new DesignatedPoint(tokens[2], pos));
	}
}

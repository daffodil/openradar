/**
 * 
 */
package de.knewcleus.openradar.ui.labels;

public enum Justification {
	LEADING,
	CENTER,
	JUSTIFY,
	TRAILING
}
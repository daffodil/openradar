package de.knewcleus.openradar.ui.plaf;

import javax.swing.plaf.ComponentUI;

public abstract class VerticalScrollPaneUI extends ComponentUI {
	protected final static String propertyPrefix="VerticalScrollPane";
	
	protected String getPropertyPrefix() {
		return propertyPrefix;
	}
}

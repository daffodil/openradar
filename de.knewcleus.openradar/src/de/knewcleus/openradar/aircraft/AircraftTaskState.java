package de.knewcleus.openradar.aircraft;

public enum AircraftTaskState {
	NOT_CONCERNED,
	PENDING,
	PENDING_IN,
	ASSUMED,
	ASSUMED_OUT;
}

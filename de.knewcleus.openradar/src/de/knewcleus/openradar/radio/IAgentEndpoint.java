package de.knewcleus.openradar.radio;

public interface IAgentEndpoint extends IEndpoint {

	public abstract String getRegion();

	public abstract String getStation();

}
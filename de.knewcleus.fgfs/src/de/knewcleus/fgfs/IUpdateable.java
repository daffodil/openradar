package de.knewcleus.fgfs;

public interface IUpdateable {
	public void update(double dt);
}

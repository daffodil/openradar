package de.knewcleus.openradar.ui;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JFrame;

import de.knewcleus.fgfs.Updater;
import de.knewcleus.fgfs.location.Ellipsoid;
import de.knewcleus.fgfs.location.GeodToCartTransformation;
import de.knewcleus.fgfs.location.LocalSphericalProjection;
import de.knewcleus.fgfs.util.GeometryConversionException;

import de.knewcleus.openradar.aircraft.AircraftStateManager;
import de.knewcleus.openradar.aircraft.BuddySquawkAllocator;
import de.knewcleus.openradar.aircraft.CorrelationDatabase;
import de.knewcleus.openradar.aircraft.ICorrelationDatabase;
import de.knewcleus.openradar.aircraft.ISquawkAllocator;
import de.knewcleus.openradar.sector.Sector;
import de.knewcleus.openradar.ui.rpvd.RadarPlanViewDisplay;
import de.knewcleus.openradar.ui.rpvd.RadarPlanViewSettings;
import de.knewcleus.openradar.vessels.IPositionDataProvider;
import de.knewcleus.openradar.vessels.TrackManager;

import de.knewcleus.openradar.vessels.fgmp.ATCClient;
import de.knewcleus.openradar.vessels.fgmp.MultiPlayerClientThread;
import de.knewcleus.openradar.vessels.fgmp.FGMPAircraft;
import de.knewcleus.openradar.vessels.fgmp.FGMPRegistry;

public class RadarWorkstation {
	
	protected final Sector sector;
	protected final ISquawkAllocator squawkAllocator = new BuddySquawkAllocator(); //squawkAllocator;
	protected final ICorrelationDatabase correlationDatabase = new CorrelationDatabase(); //correlationDatabase;
	protected final TrackManager targetManager;
	protected final AircraftStateManager aircraftStateManager;
	protected final RadarPlanViewSettings radarPlanViewSettings = new RadarPlanViewSettings();
	
	//Sector sector = Sector.loadFromURL(sectorURL);

	/* Prepare radar data provider */
	protected final IPositionDataProvider radarDataProvider;
	//ISquawkAllocator squawkAllocator = new BuddySquawkAllocator();
	//ICorrelationDatabase correlationDatabase = new CorrelationDatabase();
	
	/* Use multiplayer data */
	protected final GeodToCartTransformation geodToCartTransformation = new GeodToCartTransformation(Ellipsoid.WGS84);
	protected final FGMPRegistry registry;// = new FGMPRegistry(squawkAllocator, correlationDatabase);
	
	protected ATCClient<FGMPAircraft> multiplayerClient;// = new ATCClient<FGMPAircraft>(registry, "obsKSFO", geodToCartTransformation.backward(sector.getInitialCenter()));
	protected MultiPlayerClientThread multiplayerClientThread; // = new Thread(multiplayerClient, "FlightGear Multiplayer Protocol Handler");
	Updater multiplayerUpdater;// = new Updater(multiplayerClient, 500);
	//multiplayerClientThread.setDaemon(true);
	// TODO: this starts up the MP server, need to make this a connect/disconnect
	//multiplayerClientThread.start();
	//multiplayerUpdater.start();
	//radarDataProvider = registry;
	
	/* Globally provided windows */
	protected final List<WorkstationGlobalFrame> globalFrames = new ArrayList<WorkstationGlobalFrame>();
	
	/* The set of desktops */
	protected final List<RadarDesktop> desktops = new ArrayList<RadarDesktop>();

	protected void init_mp(){
		try{
			multiplayerClient = new ATCClient<FGMPAircraft>(registry, "obsKSFO", geodToCartTransformation.backward(sector.getInitialCenter()));
		}catch(IOException e){
			System.out.print("OOOPS");
		}
		multiplayerClientThread = new MultiPlayerClientThread(multiplayerClient, "FlightGear Multiplayer Protocol Handler");
		multiplayerUpdater = new Updater(multiplayerClient, 500);
		//multiplayerClientThread.setDaemon(true);
		run_mp(true);
		//multiplayerClientThread.start();
		
		multiplayerUpdater.start();
		System.out.print("OK");
	}
	
	public void run_mp(boolean enabled){
		System.out.printf("SS= %s", enabled);
		if(enabled){
			multiplayerClientThread.start();
		}else{
			multiplayerClientThread.stop();
		}
		//if(enabled){
			
			//multiplayerClientThread.start(); //multiplayerUpdater.start();
		//}else{
			//multiplayerClientThread.stop();
			//multiplayerClientThread.start();multiplayerUpdater.stop();
		//}
	}
	
	public RadarWorkstation(Sector sector) throws GeometryConversionException { //, IPositionDataProvider radarDataProvider, ISquawkAllocator squawkAllocator, ICorrelationDatabase correlationDatabase) throws GeometryConversionException {
		this.sector = sector;
		//this.squawkAllocator = squawkAllocator;
		//this.correlationDatabase = correlationDatabase;
		registry = new FGMPRegistry(squawkAllocator, correlationDatabase);
		radarDataProvider = registry;
		
		init_mp();
		
		radarPlanViewSettings.setRange(sector.getDefaultXRange());
		targetManager = new TrackManager(correlationDatabase);
		targetManager.addPositionDataProvider(radarDataProvider);
		aircraftStateManager = new AircraftStateManager();
		
		/* Make sure to register all global frames before creating the desktops */
		final RadarPlanViewDisplay radarPlanViewDisplay = new RadarPlanViewDisplay(this, new LocalSphericalProjection(sector.getInitialCenter()));
		registerGlobalFrame(radarPlanViewDisplay);
		
		//final FGCOMFrame comFrame = new FGCOMFrame(this);
		//registerGlobalFrame(comFrame);
		
		/* Create a desktop on every device */
		/* For now this is only one radar screen - pedro */
		GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
		//for (GraphicsDevice graphicsDevice: graphicsEnvironment.getScreenDevices()) {
			RadarDesktop desktop;
			
			desktop = new RadarDesktop(graphicsDevice.getDefaultConfiguration(), this);
			desktop.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			desktops.add(desktop);
		//}
		
		final RadarDesktop primaryDesktop = desktops.get(0);
		
		/* Place the global frames on the first desktop */
		for (WorkstationGlobalFrame frame: globalFrames) {
			frame.acquire(primaryDesktop);
		}
	}
	
	public void setVisible(boolean visible) {
		for (RadarDesktop desktop: desktops) {
			desktop.setVisible(visible);
		}
	}
	
	public void loadPreferences(Preferences prefs) {
		// TODO
	}
	
	public void savePreferences(Preferences prefs) {
		// TODO
	}
	
	public Sector getSector() {
		return sector;
	}
	
	public ICorrelationDatabase getCorrelationDatabase() {
		return correlationDatabase;
	}
	
	public ISquawkAllocator getSquawkAllocator() {
		return squawkAllocator;
	}
	
	public TrackManager getTargetManager() {
		return targetManager;
	}
	
	public AircraftStateManager getAircraftStateManager() {
		return aircraftStateManager;
	}
	
	public RadarPlanViewSettings getRadarPlanViewSettings() {
		return radarPlanViewSettings;
	}
	
	public void registerGlobalFrame(WorkstationGlobalFrame frame) {
		globalFrames.add(frame);
	}
	
	public List<WorkstationGlobalFrame> getGlobalFrames() {
		return globalFrames;
	}
	
	public void unregisterGlobalFrame(WorkstationGlobalFrame frame) {
		globalFrames.remove(frame);
	}
}

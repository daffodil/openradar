package de.knewcleus.openradar.ui.rpvd;

public enum StandardLabelDistance {
	SMALL,
	MEDIUM,
	LONG;
}

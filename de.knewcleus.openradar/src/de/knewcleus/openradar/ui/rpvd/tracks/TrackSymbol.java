package de.knewcleus.openradar.ui.rpvd.tracks;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import de.knewcleus.fgfs.location.IMapProjection;
import de.knewcleus.fgfs.location.Position;
import de.knewcleus.openradar.autolabel.DisplayObject;
import de.knewcleus.openradar.ui.Palette;
import de.knewcleus.openradar.ui.core.WorkObjectSymbol;
import de.knewcleus.openradar.ui.map.RadarMapPanel;
import de.knewcleus.openradar.vessels.Track;
import de.knewcleus.openradar.vessels.Vessel;

/* The Info Box with aero details */

public class TrackSymbol extends WorkObjectSymbol implements DisplayObject {
	protected final Track associatedTrack;
	protected Point2D devicePosition=new Point2D.Double();

	public TrackSymbol(Track associatedTrack) {
		this.associatedTrack = associatedTrack;
	}
	
	public Track getAssociatedTrack() {
		return associatedTrack;
	}
	
	@Override
	public Vessel getAssociatedObject() {
		return associatedTrack.getCorrelatedVessel();
	}
	
	@Override
	public Rectangle2D getBounds() {
		// TODO: if we have an correlated vessel, ask the vessel for the bounds
		return TrackDisplayHelper.getTrackSymbolBounds(devicePosition);
	}

	@Override
	public void paintElement(Graphics2D g) {
		// TODO: if we have a correlated vessel, tell the vessel to paint
		g.setColor(Palette.TRACK);
		TrackDisplayHelper.drawTrackSymbol(g, devicePosition);
	}

	@Override
	public void validate() {
		invalidate();
		final RadarMapPanel mapPanel = (RadarMapPanel) getDisplayComponent();
		final IMapProjection projection = mapPanel.getProjection();
		final AffineTransform deviceXForm = mapPanel.getMapTransformation();
		
		final Position realPosition = getAssociatedTrack().getPosition();
		final Point2D mapPosition = projection.forward(realPosition);
		deviceXForm.transform(mapPosition, devicePosition);
		validateDependents();
		invalidate();
	}

	@Override
	public boolean isHit(Point2D position) {
		return getBounds().contains(position);
	}

	@Override
	public Rectangle2D getBounds2D() {
		return getBounds();
	}

	@Override
	public double getPriority() {
		return 10;
	}
	
	@Override
	public Point2D getRelativeHookPoint(double vx, double vy) {
		// TODO: if we have an correlated vessel, ask the vessel for the hook point
		return TrackDisplayHelper.getRelativeTrackSymbolHookPoint(vx, vy);
	}
}

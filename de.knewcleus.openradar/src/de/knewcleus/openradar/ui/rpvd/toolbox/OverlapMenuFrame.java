package de.knewcleus.openradar.ui.rpvd.toolbox;

import java.awt.Dimension;
//import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
//import javax.swing.JToggleButton;
//import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

import de.knewcleus.openradar.ui.RadarWorkstation;
import de.knewcleus.openradar.ui.rpvd.RadarPlanViewDisplay;
import de.knewcleus.openradar.ui.rpvd.RadarPlanViewSettings;
import de.knewcleus.openradar.ui.rpvd.StandardLabelDistance;
import de.knewcleus.openradar.ui.rpvd.StandardLabelPosition;

public class OverlapMenuFrame extends JFrame implements PropertyChangeListener, ActionListener {
	private static final long serialVersionUID = 2239922786869601242L;


	protected final RadarPlanViewSettings radarPlanViewSettings;

	//protected final JToggleButton autoLabellingToggle = new JToggleButton("AUTO");
	
	protected final ButtonGroup autoLabelGroup = new ButtonGroup();
	protected final JRadioButton autoLabelEnabled = new JRadioButton("Enabled"); 
	protected final JRadioButton autoLabelCustom = new JRadioButton("Set Custom"); 
	
	
	//protected final JPanel bottomPanel = new JPanel();
	
	protected final ButtonGroup lengthGroup = new ButtonGroup();
	protected final JRadioButton lengthSmall = new JRadioButton("Small"); 
	protected final JRadioButton lengthMedium = new JRadioButton("Medium"); 
	protected final JRadioButton lengthLong = new JRadioButton("Large");
	
	protected final ButtonGroup positionGroup = new ButtonGroup();
	protected final JRadioButton positionButtons[] = new JRadioButton[8];
	protected final StandardLabelPosition labelPositions[] = new StandardLabelPosition[] {
		StandardLabelPosition.TOPLEFT,	
		StandardLabelPosition.TOP,	
		StandardLabelPosition.TOPRIGHT,	
		StandardLabelPosition.RIGHT,
		StandardLabelPosition.BOTTOMRIGHT,	
		StandardLabelPosition.BOTTOM,
		StandardLabelPosition.BOTTOMLEFT,	
		StandardLabelPosition.LEFT
	};
	
	public OverlapMenuFrame(RadarPlanViewDisplay radarPlanViewDisplay) {
		//super("LABEL POSITION",false,true,false,false);
		super("Markers");
		//this.radarToolbox=radarToolbox;
		setResizable(false);
		
		//RadarPlanViewDisplay radarPlanViewDisplay=radarToolbox.getRadarPlanViewDisplay();
		RadarWorkstation radarWorkstation = radarPlanViewDisplay.getWorkstation();
		radarPlanViewSettings = radarWorkstation.getRadarPlanViewSettings();
		radarPlanViewSettings.addPropertyChangeListener(this);
		
		BoxLayout mainLayout = new BoxLayout(getContentPane(), BoxLayout.Y_AXIS);
		setPreferredSize(new Dimension(200, 300));
		setLayout(mainLayout);
		
		
		//== Top Panel
		final JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
		topPanel.setBorder(new TitledBorder("Auto Label"));
		
		topPanel.add(autoLabelEnabled);
		topPanel.add(autoLabelCustom);
		
		autoLabelGroup.add(autoLabelEnabled);
		autoLabelGroup.add(autoLabelCustom);
		
		autoLabelEnabled.addActionListener(this);
		autoLabelCustom.addActionListener(this);
		
		//=== Length Panel ===
		final JPanel lengthPanel = new JPanel();
		lengthPanel.setLayout(new BoxLayout(lengthPanel, BoxLayout.Y_AXIS));
		lengthPanel.setBorder(new TitledBorder("Lengths"));
		//lengthPanel.setOpaque(true);
		
		//JLabel lengthLabel = new JLabel("Length");
		//lengthPanel.add(lengthLabel);
		lengthPanel.add(lengthSmall);
		lengthPanel.add(lengthMedium);
		lengthPanel.add(lengthLong);
		
		lengthGroup.add(lengthSmall);
		lengthGroup.add(lengthMedium);
		lengthGroup.add(lengthLong);
		
		lengthSmall.addActionListener(this);
		lengthMedium.addActionListener(this);
		lengthLong.addActionListener(this);
		
		//== Position Panel
		final JPanel positionPanel = new JPanel();
		positionPanel.setLayout(new GridBagLayout());
		positionPanel.setBorder(new TitledBorder("Position"));
		//bottomPanel.setLayout(new BoxLayout(bottomPanel,BoxLayout.Y_AXIS));
		//bottomPanel.add(lengthPanel);
		//bottomPanel.add(positionPanel);
		
		//topPanel.add(autoLabellingToggle);
		
		add(topPanel);
		add(lengthPanel);
		add(positionPanel);
		//add(bottomPanel);
		

		
		//autoLabellingToggle.addActionListener(this);

		
		GridBagConstraints gridBagConstraints=new GridBagConstraints();
		for (int i = 0; i < 8; i++) {
			positionButtons[i] = new JRadioButton("");
			positionButtons[i].setMinimumSize(new Dimension(10, 10));
			gridBagConstraints.gridx = labelPositions[i].getDx() + 1;
			gridBagConstraints.gridy = labelPositions[i].getDy() + 1;
			positionPanel.add(positionButtons[i], gridBagConstraints);
			positionGroup.add(positionButtons[i]);
			positionButtons[i].addActionListener(this);
		}
		
		updateAutolabellingStatus();
		updateLabelPosition();
		updateLabelDistance();
		
		pack();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == autoLabelEnabled) {
			radarPlanViewSettings.setAutomaticLabelingEnabled(true);
			System.out.print("AUTO ONan");
			updateAutolabellingStatus();
			
		}else if (e.getSource() == autoLabelCustom) {
			radarPlanViewSettings.setAutomaticLabelingEnabled(false);
			System.out.print("AUTO OFF\n");
			updateAutolabellingStatus();
			
		//== Lengths
		} else if (e.getSource() == lengthSmall) {
			radarPlanViewSettings.setStandardLabelDistance(StandardLabelDistance.SMALL);
			System.out.print("Snall\n");
			
		} else if (e.getSource() == lengthMedium) {
			radarPlanViewSettings.setStandardLabelDistance(StandardLabelDistance.MEDIUM);
			System.out.print("Med\n");
			
		} else if (e.getSource() == lengthLong) {
			radarPlanViewSettings.setStandardLabelDistance(StandardLabelDistance.LONG);
			System.out.print("Long\n");
			
		} else {
			for (int i = 0; i < positionButtons.length; i++) {
				if (e.getSource()==positionButtons[i]) {
					radarPlanViewSettings.setStandardLabelPosition(labelPositions[i]);
				}
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String propertyName = evt.getPropertyName();
		
		if (propertyName.equals(RadarPlanViewSettings.IS_AUTOMATIC_LABELING_ENABLED_PROPERTY)) {
			updateAutolabellingStatus();
			
		} else if (propertyName.equals(RadarPlanViewSettings.STANDARD_LABEL_POSITION_PROPERTY)) {
			updateLabelPosition();
			
		} else if (propertyName.equals(RadarPlanViewSettings.STANDARD_LABEL_DISTANCE_PROPERTY)) {
			updateLabelDistance();
		}
	}
	
	private void updateAutolabellingStatus() {
		final boolean status = radarPlanViewSettings.isAutomaticLabelingEnabled();
		//autoLabellingToggle.setSelected(status);
		autoLabelEnabled.setSelected(status);
		//bottomPanel.setVisible(!status);
		invalidate();
		pack();
	}
	
	private void updateLabelPosition() {
		final StandardLabelPosition position=radarPlanViewSettings.getStandardLabelPosition();
		System.out.printf("Update POSTITION: %s\n", position);
		for (int i = 0; i < labelPositions.length; i++) {
			if (labelPositions[i] == position) {
				positionButtons[i].setSelected(true);
			}
		}
	}
	
	private void updateLabelDistance() {
		final StandardLabelDistance distance = radarPlanViewSettings.getStandardLabelDistance();
		
		switch (distance) {
			case SMALL:
				lengthSmall.setSelected(true);
				break;
				
			case MEDIUM:
				lengthMedium.setSelected(true);
				break;
				
			case LONG:
				lengthLong.setSelected(true);
				break;
		}
	}
}

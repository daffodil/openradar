package de.knewcleus.fgfs.navaids.ead;

import java.awt.Shape;

import org.w3c.dom.Element;

import de.knewcleus.fgfs.location.Position;
import de.knewcleus.fgfs.navaids.DBParserException;
import de.knewcleus.fgfs.navaids.DesignatedPoint;
import de.knewcleus.fgfs.navaids.NavDB;

public class DesignatedPointsParser extends AbstractSDOParser {
	protected final NavDB navDB;

	public DesignatedPointsParser(NavDB navDB, Shape geographicBounds) {
		super(geographicBounds);
		this.navDB = navDB;
	}

	@Override
	public void processRecord(Element record) throws DBParserException {
		requireField(record, "codeId");
		double lat=getLatitude(record, "geoLat");
		double lon=getLongitude(record, "geoLong");
		
		if (!isInRange(lon, lat))
			return;
		
		String name=getFieldValue(record, "codeId");
		Position pos=new Position(lon,lat,0.0);
		navDB.addRecord(new DesignatedPoint(name, pos));
	}

}

package de.knewcleus.fgfs.navaids;

import de.knewcleus.fgfs.location.Position;

public class AbstractNavRecord implements NavRecord {
	
	protected final String id;
	protected final Position position;
	protected final String xtype;
	
	public AbstractNavRecord(String xtype, String id, Position position) {
		this.xtype = xtype;
		this.id = id;
		this.position = position;
		
	}
	
	public String getID() {
		return id;
	}
	public String getXType() {
		return xtype;
	}
	public Position getPosition() {
		return position;
	}
	
	@Override
	public String toString() {
		return id + position.toString();
	}
}

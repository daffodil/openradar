package de.knewcleus.openradar.ui.rpvd.toolbox;

import java.awt.GridBagLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.border.TitledBorder;

import de.knewcleus.openradar.ui.RadarWorkstation;
import de.knewcleus.openradar.ui.rpvd.RadarPlanViewDisplay;
import de.knewcleus.openradar.ui.rpvd.RadarPlanViewSettings;

public class SpeedAndTrackPanel extends JPanel implements PropertyChangeListener {
	private static final long serialVersionUID = 2264072614359267276L;
	
	//protected final RadarToolbox radarToolbox;
	protected final RadarPlanViewSettings settings;

	protected final Integer[] speedVectorLengths = new Integer[] { 0,1,2,3,4,5 };
	//protected final JLabel speedVectorLabel = new JLabel("Speed Vector");
	//protected final JPanel speedVectorPanel = new JPanel();
	protected final JToggleButton[] speedVectorButtons = new JToggleButton[6];
	
	protected final Integer[] trackHistoryLengths = new Integer[] { 0,3,5,7 };
	//protected final JLabel trackHistoryLabel = new JLabel("Track History");
	//protected final JPanel trackHistoryPanel = new JPanel();
	protected final JToggleButton[] trackHistoryButtons = new JToggleButton[4];
	
	public SpeedAndTrackPanel(final RadarPlanViewDisplay radarPlanViewDisplay) {
		//this.radarToolbox=radarToolbox;
		
		setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		
		JPanel speedPanel = new JPanel();
		speedPanel.setLayout(new BoxLayout(speedPanel, BoxLayout.X_AXIS));
		speedPanel.setBorder(new TitledBorder("Speed"));
		add(speedPanel);
		
		JPanel tracksPanel = new JPanel();
		tracksPanel.setLayout(new BoxLayout(tracksPanel, BoxLayout.X_AXIS));
		tracksPanel.setBorder(new TitledBorder("Track History"));
		add(tracksPanel);
		
		//add(trackHistoryLabel);
		//add(speedVectorPanel);
		//add(trackHistoryPanel);
		
		//final RadarPlanViewDisplay radarPlanViewDisplay=radarToolbox.getRadarPlanViewDisplay();
		final RadarWorkstation workstation = radarPlanViewDisplay.getWorkstation();
		settings = workstation.getRadarPlanViewSettings();
		
		ActionListener speedVectorChange = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int minutes = Integer.parseInt(e.getActionCommand());
				settings.setSpeedVectorMinutes(minutes);
			}
		};
		
		ButtonGroup speedVectorGroup = new ButtonGroup();
		//speedVectorPanel.setLayout(new GridBagLayout());
		for (int i=0; i < speedVectorLengths.length; i++) {
			String label = Integer.toString(speedVectorLengths[i]);
			JToggleButton button = new JToggleButton(label);
			speedVectorButtons[i] = button;
			button.setActionCommand(label);
			button.addActionListener(speedVectorChange);
			speedVectorGroup.add(button);
			speedPanel.add(button);
			button.setSize(20, 20);
			//Font font = button.getFont();
			//font.
			//button.setFont(font )
			if (settings.getSpeedVectorMinutes() == speedVectorLengths[i]) {
				button.setSelected(true);
			}
		}
		
		
		ActionListener trackHistoryChange = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int trackHistoryLength = Integer.parseInt(e.getActionCommand());
				settings.setTrackHistoryLength(trackHistoryLength);
			}
		};
		ButtonGroup trackHistoryGroup = new ButtonGroup();
		//trackHistoryPanel.setLayout(new GridBagLayout());
		for (int i = 0; i < trackHistoryLengths.length; i++) {
			String label = Integer.toString(trackHistoryLengths[i]);
			JToggleButton button = new JToggleButton(label);
			trackHistoryButtons[i] = button;
			button.setActionCommand(label);
			button.addActionListener(trackHistoryChange);
			trackHistoryGroup.add(button);
			tracksPanel.add(button);
			
			if (settings.getTrackHistoryLength() == trackHistoryLengths[i]) {
				button.setSelected(true);
			}
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(RadarPlanViewSettings.SPEED_VECTOR_MINUTES_PROPERTY)) {
			for (int i = 0; i < speedVectorLengths.length; i++) {
				if (settings.getSpeedVectorMinutes() == speedVectorLengths[i]) {
					speedVectorButtons[i].setSelected(true);
				}
			}
		} else if (evt.getPropertyName().equals(RadarPlanViewSettings.TRACK_HISTORY_LENGTH_PROPERTY)) {
			for (int i = 0; i < speedVectorLengths.length; i++) {
				if (settings.getTrackHistoryLength() == trackHistoryLengths[i]) {
					trackHistoryButtons[i].setSelected(true);
				}
			}
		}
	}
}

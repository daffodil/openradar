package de.knewcleus.fgfs.geodata;

public interface IDatabaseRow {
	public abstract Object getField(int index);
}

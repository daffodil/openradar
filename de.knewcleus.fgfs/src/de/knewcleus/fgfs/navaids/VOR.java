package de.knewcleus.fgfs.navaids;

import de.knewcleus.fgfs.location.Position;

public class VOR extends AbstractNavRecord {
	
	protected final String name;
	protected final double frequency;
	protected final double range;
	protected final double variation;
	
	public VOR(String id, Position position, String name, double frequency, double range, double variation) {
		super("VOR", id, position);
		this.name = name; 
		this.frequency = frequency;
		this.range = range;
		this.variation = variation;
	}
	
	public double getVariation() {
		return variation;
	}
}

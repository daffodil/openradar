package de.knewcleus.fgfs.navaids;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.knewcleus.fgfs.location.Position;

public class NavDB { 
	
	protected Map<String, Set<NavRecord>> navIndex = new HashMap<String, Set<NavRecord>>();
	protected Set<NavRecord> navItems = new HashSet<NavRecord>();
	
	public void addRecord(NavRecord rec) {
		Set<NavRecord> navSet;
		
		if (navIndex.containsKey(rec.getID())) {
			navSet = navIndex.get(rec.getID());
		} else {
			navSet = new HashSet<NavRecord>();
			navIndex.put(rec.getID(), navSet);
		}
		//System.out.print(fix.getID() + "\n");
		navSet.add(rec);
		navItems.add(rec);
	}
	
	public Set<NavRecord> getRecords() {
		return Collections.unmodifiableSet(navItems);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends NavRecord> Set<T> getRecords(Class<T> clazz) {
		
		Set<T> selectedRecords = new HashSet<T>();
		
		for (NavRecord n: navItems) {
			if (clazz.isInstance(n))
				selectedRecords.add((T)n);
		}
		return selectedRecords;
	}
	
	
	@SuppressWarnings("unchecked")
	public <T extends NavRecord> Set<T> getFixes(Class<T> clazz) {
		
		Set<T> selectedFixes = new HashSet<T>();
		
		for (NavRecord n: navItems) {
			if (clazz.isInstance(n))
				selectedFixes.add((T)n);
		}
		
		return selectedFixes;
	}
	
	public NavRecord findNearestFix(String name, Position reference) {
		return findNearestFix(name, reference, NavRecord.class);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends NavRecord> T findNearestFix(String name, Position reference, Class<T> clazz) {
		if (!navIndex.containsKey(name))
			return null; // TODO: throw NoSuchFixException
		Set<NavRecord> fixSet = navIndex.get(name);
		
		NavRecord nearestFix = null;
		double nearestDistSquared = 0.0;
		for (NavRecord fix: fixSet) {
			if (nearestFix == null) {
				nearestFix = fix;
				continue;
			}
			
			Position fixPos = fix.getPosition();
			
			double dx, dy;
			dx = fixPos.getX() - reference.getX();
			dy = fixPos.getY() - reference.getY();
			
			double distSquared = dx * dx + dy * dy;
			
			if (distSquared<nearestDistSquared) {
				nearestFix = fix;
				nearestDistSquared = distSquared;
			}
		}
		
		return (T)nearestFix;
	}
}

package de.knewcleus.openradar.radio;

public interface IEndpoint extends IChannelListener {
	public String getCallsign();
}

package de.knewcleus.fgfs.navaids.xplane;

import static de.knewcleus.fgfs.Units.DEG;
import static de.knewcleus.fgfs.Units.FT;
import static de.knewcleus.fgfs.Units.MHZ;
import static de.knewcleus.fgfs.Units.NM;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

import java.awt.Shape;

import de.knewcleus.fgfs.location.Position;
import de.knewcleus.fgfs.navaids.DBParserException;
import de.knewcleus.fgfs.navaids.NavDB;
import de.knewcleus.fgfs.navaids.VOR;

public class NavParser extends AbstractXPlaneParser {
	protected final NavDB navDB;

	public NavParser(NavDB navDB, Shape geographicBounds) {
		super(geographicBounds);
		this.navDB = navDB;
	}
	
	@Override
	protected void processRecord(String line) throws DBParserException {
		
		String[] tokens = line.split("\\s+", 9);
		
		//= Parse lat/lon
		double lon, lat;
		lat = parseDouble(tokens[1]);
		lon = parseDouble(tokens[2]);
		
		if (!isInRange(lon, lat))
			return; // Out of range so not required.. ? TODO maybe reload with new ranges
		
		int type = parseInt(tokens[0]);
		double elev = parseDouble(tokens[3]) * FT;
		int freq = parseInt(tokens[4]);
		double range = parseInt(tokens[5]) * NM;
		String id = tokens[7];
		String name = tokens[8];
		
		Position pos = new Position(lon, lat, elev);
		
		
		switch (type) {
		case 3:
			//System.out.printf("%s %s %s\n", id, pos, name);
			navDB.addRecord(new VOR(id, pos, name, 
										freq * MHZ / 100.0,	
										range, 
										parseDouble(tokens[6]) * DEG
									));
			break;
		}
	}
}

package de.knewcleus.openradar.ui.rpvd.toolbox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowStateListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import sun.awt.WindowClosingListener;

import de.knewcleus.openradar.ui.map.IMapLayer;
import de.knewcleus.openradar.ui.map.RadarMapPanel;
import de.knewcleus.openradar.ui.rpvd.RadarPlanViewDisplay;
/** Displays the Map Layers options
 * 
 *
 */
public class MapMenuFrame extends JFrame {
	private static final long serialVersionUID = 6741748501762906857L;
	
	//protected final RadarToolbox radarToolbox;
	
	protected final JPanel checkboxPanel = new JPanel();
	//protected final JScrollPane scrollPane = new JScrollPane(checkboxPanel);
	
	//public WindowStateListener
	
	public MapMenuFrame(RadarPlanViewDisplay radarPlanViewDisplay) {
		//super(null,false,false,false,false);
		//this.radarToolbox=radarToolbox;
		super("Map");
		
		setResizable(false);
		//set(JRootPane.PLAIN_DIALOG);  
		//setUndecorated(true);
		//final RadarPlanViewDisplay radarPlanViewDisplay=radarToolbox.getRadarPlanViewDisplay();
		
		final RadarMapPanel radarMapPanel = radarPlanViewDisplay.getRadarMapPanel();
		
		for (final IMapLayer layer: radarMapPanel.getMapLayers()) {
			final JCheckBox layerCheckbox = new JCheckBox(layer.getName());
			layerCheckbox.setSelected(layer.isVisible());
			layerCheckbox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					layer.setVisible(layerCheckbox.isSelected());
					radarMapPanel.repaint();
				}
			});
			checkboxPanel.add(layerCheckbox);
		}
		
		checkboxPanel.setLayout(new BoxLayout(checkboxPanel, BoxLayout.Y_AXIS));
		
		setContentPane(checkboxPanel);
		
		pack();
	}
}

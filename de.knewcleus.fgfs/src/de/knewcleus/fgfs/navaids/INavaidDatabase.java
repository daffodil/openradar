package de.knewcleus.fgfs.navaids;


public interface INavaidDatabase {

	public abstract NavDB getNavDB();

	public abstract AirwayDB getAirwayDB();

}